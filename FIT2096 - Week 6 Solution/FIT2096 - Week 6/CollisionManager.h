#ifndef COLLISION_MANAGER_H
#define COLLISION_MANAGER_H

#include <vector>
#include "Collisions.h"
#include "Bullet.h"
#include "Player.h"
#include "Monster.h"
#include "Capsule.h"
#include "Wall.h"
#include "Ammo.h"

#define MAX_ALLOWED_COLLISIONS 2048

class CollisionManager
{
private:
	std::vector<Bullet*>* m_bullets;
	std::vector<Bullet*>* E_bullets;
	std::vector<Monster*>* m_monsters;
	std::vector<Capsule*>* m_capsules;
	std::vector<Ammo*>* m_ammos;
	std::vector<Wall*>* m_walls;
	Player** m_player;
	GameObject* m_currentCollisions[MAX_ALLOWED_COLLISIONS];

	// We need to know what objects were colliding last frame so we can determine if a collision has just begun or ended
	GameObject* m_previousCollisions[MAX_ALLOWED_COLLISIONS];

	int m_nextCurrentCollisionSlot;

	// Check if we already know about two objects colliding
	bool ArrayContainsCollision(GameObject* arrayToSearch[], GameObject* first, GameObject* second);

	// Register that a collision has occurred
	void AddCollision(GameObject* first, GameObject* second);

	// Collision check helpers
	void BulletToMonster();
	void BulletToPlayer();
	void PlayerToMonster();
	void PlayerToWall();
	void PlayerToCapsule();
	void PlayerToAmmo();
	void MonsterToWall();

public:
	CollisionManager(std::vector<Bullet*>* m_bullets, std::vector<Bullet*>* E_bullets, std::vector<Monster*>* m_monsters, Player** player, std::vector<Capsule*>* m_capsules, std::vector<Wall*>* m_walls, std::vector<Ammo*>* m_ammos);
	void CheckCollisions();

};

#endif