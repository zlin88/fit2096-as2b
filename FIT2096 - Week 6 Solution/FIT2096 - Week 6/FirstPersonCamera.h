#ifndef FIRST_PERSON_CAMERA_H
#define FIRST_PERSON_CAMERA_H

#include "Camera.h"
#include "InputController.h"

class FirstPersonCamera : public Camera
{
private:
	InputController * m_input;

	float m_moveSpeed;
	float m_rotationSpeed;
	float m_heightChangeSpeed;
	float m_heading;
	float m_pitch;

public:
	FirstPersonCamera(InputController* input, Vector3 startPos);
	void Update(float timestep);
};

#endif