#ifndef BULLET_H
#define BULLET_H

#include"GameObject.h"
class Bullet: public GameObject {
protected:
	//bullet range
	float m_range;
	//bullet speed
	float m_speed;
	//damage for bullet
	int m_damage;
	//start position
	Vector3 m_Sposition;
	//End Position for the longest distance bullet can arrive
	Vector3 m_Eposition;
	Matrix heading;
	bool disappear;
	CBoundingBox m_boundingBox;

public:
	Bullet(Mesh* mesh, Shader* shader, Texture* texture, Vector3 position);

	void Update(float timestep);
	Vector3 GetStartPoint() { return m_Sposition; }
	void SetSpeed(float sp) { m_speed = sp; }
	CBoundingBox GetBounds() { return m_boundingBox; }
	void OnMonsterCollisionEnter();
	void OnMonsterCollisionStay();
	void OnMonsterCollisionExit();
	int GetDamage() { return m_damage; }
	bool GetDisappear() { return disappear; }

	void OnPlayerCollisionEnter();
	void OnPlayerCollisionStay();
	void OnPlayerCollisionExit();

};
#endif