#ifndef AMMO_H
#define AMMO_H

#include"GameObject.h"
class Ammo : public GameObject {
protected:
	//bullet range
	int amount;
	//bullet speed
	bool disappear;
	//start position
	Vector3 m_Sposition;
	
	CBoundingBox m_boundingBox;

public:
	Ammo(Mesh* mesh, Shader* shader, Texture* texture, Vector3 position);

	void Update(float timestep);
	void Render(Direct3D* renderer, Camera* cam);
	CBoundingBox GetBounds() { return m_boundingBox; }

	int GetAmount() { return amount; }
	bool GetDisappear() { return disappear; }

	void OnPlayerCollisionEnter();
	void OnPlayerCollisionStay();
	void OnPlayerCollisionExit();

};
#endif