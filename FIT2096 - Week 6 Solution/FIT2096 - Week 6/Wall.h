#ifndef WALL_H
#define WALL_H

#include "GameObject.h"
#include "InputController.h"
class Wall : public GameObject
{
private:
	CBoundingBox m_boundingBox;
public:
	Wall(Mesh* mesh, Shader* shader,Texture* texture, Vector3 position, float scale);
	void Update(float timestep);
	CBoundingBox GetBounds() { return m_boundingBox; }

	void OnPlayerCollisionEnter();
	void OnPlayerCollisionStay();
	void OnPlayerCollisionExit();
};

#endif