#include "SpinningObject.h"

SpinningObject::SpinningObject() : GameObject()
{
	m_spinSpeed = 1.0f;
}

SpinningObject::SpinningObject(Mesh* mesh, Shader* shader, float spinSpeed) : 
	GameObject(mesh, shader)
{
	m_spinSpeed = spinSpeed;
}

SpinningObject::SpinningObject(Mesh* mesh, Shader* shader, Vector3 position, float spinSpeed) : 
	GameObject(mesh, shader, position)
{
	m_spinSpeed = spinSpeed;
}

SpinningObject::SpinningObject(Mesh* mesh, Shader* shader, Texture* texture, float spinSpeed) :
	GameObject(mesh, shader, texture)
{
	m_spinSpeed = spinSpeed;
}

SpinningObject::SpinningObject(Mesh* mesh, Shader* shader, Texture* texture, Vector3 position, float spinSpeed) :
	GameObject(mesh, shader, texture, position)
{
	m_spinSpeed = spinSpeed;
}

SpinningObject::~SpinningObject() { }

void SpinningObject::Update(float timestep)
{
	// Constantly spin around the Y axis
	m_rotY += m_spinSpeed * timestep;
}