#ifndef PLAYER_H
#define PLAYER_H

#include "GameObject.h"
#include "InputController.h"
#include "Bullet.h"
#include "Ammo.h"
class Player : public GameObject
{
private:
	InputController* m_input;

	// For distance travelled calculation
	Vector3 m_previousPosition;
	float m_distanceTravelled;

	float m_moveSpeed;
	float m_turnSpeed;
	bool shoot;
	//battle variable
	int m_HP;
	int m_attack;
	int m_defence;
	int m_point = 0;
	int monster_beat = 0;
	CBoundingBox m_boundingBox;
	bool pass;
	bool getshooted;
	float change;
	int ammo;
	bool collide;
public:
	Player(Mesh* mesh, Shader* shader, Vector3 position, InputController* input, int atk, int def, int hp);

	void Update(float timestep);
	// The Game class will use these to determine if the game should end
	int GetAttack() { return m_attack; }
	int GetAmmo() { return ammo; }
	int GetDefence() { return m_defence; }
	int GetHp() { return m_HP; }
	int GetPoint() { return m_point; }
	int GetBeat() { return monster_beat; }
	bool Shoot() { return shoot; }
	bool Collide() { return collide; }
	bool GetShooted() { return getshooted; }
	void SetAttack(int atk) { m_attack = atk; }
	void SetDefence(int def) { m_defence = def; }
	void SetHp(int hp) {
		m_HP = hp;
		if (hp < 0) { m_HP = 0; }
		if (hp > 50) { m_HP = 50; }

	}
	void SetBeat(int beat) { monster_beat = beat; }
	void SetAmmo(int am) { ammo = am; if (ammo > 60) { ammo = 60; } }
	void SetPoint(int point) { m_point = point; }
	CBoundingBox GetBounds() { return m_boundingBox; }

	float GetDistanceTravelled() { return m_distanceTravelled; }


	//collision with capsule
	void OnCapsuleCollisionEnter();
	void OnCapsuleCollisionStay();
	void OnCapsuleCollisionExit();
	//collision with ammoblock
	void OnAmmoCollisionEnter(Ammo* am);
	void OnAmmoCollisionStay(Ammo* am);
	void OnAmmoCollisionExit(Ammo* am);


	//collision with monster
	void OnMonsterCollisionEnter();
	void OnMonsterCollisionStay();
	void OnMonsterCollisionExit();



	//collision with wall
	void OnWallCollisionEnter();
	void OnWallCollisionStay();
	void OnWallCollisionExit();


	//collision with bullet
	void OnBulletCollisionEnter(Bullet* bullet);
	void OnBulletCollisionStay(Bullet* bullet);
	void OnBulletCollisionExit(Bullet* bullet);
};

#endif