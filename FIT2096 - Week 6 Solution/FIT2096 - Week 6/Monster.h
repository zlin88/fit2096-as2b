#ifndef MONSTER_H
#define MONSTER_H

#include "GameObject.h"
#include "InputController.h"
#include<time.h>
#include "Bullet.h"
class Monster : public GameObject
{

private:
	int m_HP;
	int points;
	int m_defence;
	int movement;
	float m_moveSpeed;
	InputController* m_input;
	Vector3 m_lookAtTarget;		
	Matrix m_view;				//This stores our final view matrix
	Vector3 randomPosition;
	float m_heading;
	bool shoot;
	float shootInterval;
	bool pass;
	CBoundingBox m_boundingBox;
	float start;
public:
	Monster(Mesh* mesh, Shader* shader, Texture* texture, Vector3 position, int move, Vector3 Target, InputController* input, int Point);
	~Monster();
	void Render(Direct3D* renderer, Camera* cam);
	void Update(float timestep);
	//mutator
	void SetPoints(int pt) { points = pt; }
	void SetHp(int hp) { m_HP = hp; }
	void SetDefence(int def) { m_defence = def; }
	void SetPowerLevel(int move) { movement = move; }

	//accessor
	int GetPoints(){ return points; }
	int GetDefence() { return m_defence; }
	int GetHp() { return m_HP; };
	int GetPowerlevel() { return movement; };
	bool Shoot() { return shoot; }

	CBoundingBox GetBounds() { return m_boundingBox; }


	void OnBulletCollisionEnter(Bullet* bullet);
	void OnBulletCollisionStay(Bullet* bullet);
	void OnBulletCollisionExit(Bullet* bullet);

	void OnPlayerCollisionEnter();
	void OnPlayerCollisionStay();
	void OnPlayerCollisionExit();

	void OnWallCollisionEnter();
	void OnWallCollisionStay();
	void OnWallCollisionExit();
};

#endif