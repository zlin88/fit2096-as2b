#include "Wall.h"



Wall::Wall(Mesh* mesh, Shader* shader, Texture* texture, Vector3 position, float scale) : GameObject(mesh, shader, texture, position, scale) 
{

}


void  Wall::Update(float timestep) {

	m_boundingBox = CBoundingBox(m_position + m_mesh->GetMin(), m_position + m_mesh->GetMax());

}


void Wall::OnPlayerCollisionEnter()
{


}

void Wall::OnPlayerCollisionStay()
{

}

void Wall::OnPlayerCollisionExit()
{


}