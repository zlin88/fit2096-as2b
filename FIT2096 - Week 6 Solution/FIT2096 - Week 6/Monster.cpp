#include "Monster.h"
#include "MathsHelper.h"



Monster::Monster(Mesh* mesh, Shader* shader, Texture* texture, Vector3 position, int move,Vector3 target,InputController* input,int pt)
	: GameObject(mesh, shader, texture, Vector3::Zero)
{
	shoot = false;
	m_input = input;
	points = pt;
	m_defence = 0;
	m_HP = rand()% 25+5;
	movement = move;
	m_position = position;
	m_scaleX = m_scaleY = m_scaleZ = 1.5f;
	m_targetPosition = target;
	m_moveSpeed = 0.1f * 15 / m_HP;
	randomPosition = Vector3(((rand() / float(RAND_MAX)) * (60 - (-60))) + (-60), 0.3f, ((rand() / float(RAND_MAX)) * (60 - (-60))) + (-60));
	m_heading = 0.0f;
	shootInterval = ((rand() / float(RAND_MAX)) * (0.2f - (0.05f))) + (0.1f);
	pass = true;
	start = 20.0f;
}




Monster::~Monster()
{
}
void Monster::Render(Direct3D* renderer, Camera* cam)
{
	if (m_mesh)
	{
		m_world = Matrix::CreateScale(m_scaleX, m_scaleY, m_scaleZ) * Matrix::CreateFromYawPitchRoll(m_rotY, m_rotX, m_rotZ) * Matrix::CreateTranslation(m_position);
		m_mesh->Render(renderer, m_shader, m_world, cam, m_texture);
	}

}

void Monster::Update(float timestep) {
	start -= 5 *timestep;
	if (start <= 0.0f) {
		m_boundingBox = CBoundingBox(m_position + m_mesh->GetMin(), m_position + m_mesh->GetMax());

		shoot = false;
		shootInterval += shootInterval * timestep;


		if (shootInterval >= 1.0f) {
			shoot = true;
			shootInterval = ((rand() / float(RAND_MAX)) * (0.3f - (0.1f))) + (0.1f);
		}


		float angle = atan2(m_targetPosition.x - m_position.x, m_targetPosition.z - m_position.z);
		if (angle < 0) {
			angle = 2 * PI - (-angle);
		}

		m_rotY = angle - 2 * PI;
		switch (movement) {
			//CHASE PLAYER
		case 1:
			m_position += (m_targetPosition - m_position) * m_moveSpeed * timestep;
			break;
			// RUN AWAY FROM PLAYER
		case 2: {
			if (pass) {
				m_moveSpeed = 0.1f * 15 / 15;
				m_position += (m_position - m_targetPosition) * m_moveSpeed * timestep * 0.5;
				break;
			}
			else
			{
				m_position -= (m_position - m_targetPosition) * m_moveSpeed * timestep;
				pass = true;

			}
		}
				// GO TO THE RANDOM POSITION
		case 3: {
			if (Vector3::Distance(m_position, randomPosition) < 10.0f)
				randomPosition = Vector3(((rand() / float(RAND_MAX)) * (60 - (-60))) + (-60), 0.3f, ((rand() / float(RAND_MAX)) * (60 - (-60))) + (-60));
			m_position += (randomPosition - m_position) * m_moveSpeed * timestep;
			break;
		}

		case 4: {
			if (pass) {
				m_heading += m_input->GetMouseDeltaX() *1.0f * timestep;
				Matrix heading = Matrix::CreateRotationY(m_heading);
				Vector3 localForward = Vector3::TransformNormal(Vector3(0, 0, 10), heading);
				Vector3 facePosition = m_targetPosition + localForward;
				m_position += (facePosition - m_position) * m_moveSpeed * timestep;
				break;
			}
			else
			{
				m_position -= (m_position - m_targetPosition) * m_moveSpeed * timestep;
				pass = true;

			}

			
		}
				// STAY REMAIN BUT TELPORT TO OTHER PLACE AFTER PLAYER COMING
		case 5: {
			if (Vector3::Distance(m_position, m_targetPosition) < 15.0f)
				randomPosition = Vector3(((rand() / float(RAND_MAX)) * (60 - (-60))) + (-60), 0.3f, ((rand() / float(RAND_MAX)) * (60 - (-60))) + (-60));
			m_position += (randomPosition - m_position) * m_moveSpeed;
			break;
		}

		}
	}
}



void Monster::OnBulletCollisionEnter(Bullet* bullet)
{
	SetHp(m_HP - bullet->GetDamage());

}

void Monster::OnBulletCollisionStay(Bullet* bullet)
{

}

void Monster::OnBulletCollisionExit(Bullet* bullet)
{


}

void Monster::OnPlayerCollisionEnter()
{


}

void Monster::OnPlayerCollisionStay()
{

}

void Monster::OnPlayerCollisionExit()
{


}

void Monster::OnWallCollisionEnter()
{
	pass = false;
}

void Monster::OnWallCollisionStay()
{
	pass = false;

}

void  Monster::OnWallCollisionExit()
{
	pass = false;

}