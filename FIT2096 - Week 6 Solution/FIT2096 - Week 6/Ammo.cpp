#include "Ammo.h"
#include "MathsHelper.h"
#include <time.h>


Ammo::Ammo(Mesh* mesh, Shader* shader, Texture* texture, Vector3 position)
	: GameObject(mesh, shader, texture, Vector3::Zero)
{
	srand(time(NULL));
	disappear = false;
	m_position = position;
	amount = rand() % 20 + 10;
	m_scaleX = m_scaleY = m_scaleZ = 6.0f;
}




void Ammo::Update(float timestep) {
	m_boundingBox = CBoundingBox(m_position + m_mesh->GetMin(), m_position + m_mesh->GetMax());

}

void Ammo::Render(Direct3D* renderer, Camera* cam)
{
	if (m_mesh)
	{

		m_world = Matrix::CreateScale(m_scaleX, m_scaleY, m_scaleZ) * Matrix::CreateFromYawPitchRoll(m_rotY, m_rotX, m_rotZ) * Matrix::CreateTranslation(m_position);
		m_mesh->Render(renderer, m_shader, m_world, cam, m_texture);
	}

}

void Ammo::OnPlayerCollisionEnter()
{
	disappear = true;

}

void   Ammo::OnPlayerCollisionStay()
{
	disappear = true;
}

void  Ammo::OnPlayerCollisionExit()
{
	disappear = true;

}

