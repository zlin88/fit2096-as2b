#include "Player.h"
#include "MathsHelper.h"
#include <iostream>
using namespace std;

Player::Player(Mesh* mesh, Shader* shader, Vector3 position, InputController* input, int atk, int def, int hp) :
GameObject(mesh, shader, position)
{
	m_HP = hp;
	m_input = input;
	m_attack = atk;
	m_defence = def;
	m_point = 0;
	monster_beat = 0;
	shoot = false;
	m_moveSpeed =15.0f;
	m_turnSpeed = 1.0f;
	pass = true;
	m_previousPosition = m_position;
	m_scaleX =  1.5f;
	m_scaleY = 1.0f;
	m_scaleZ = 0.0f;
	getshooted = false;
	change = 0.0f;
	ammo = 50;
	collide = false;
}

void Player::Update(float timestep)
{
	if (getshooted == true) { 
		change += timestep * 10.0f;
		if (change >= 10.0f) {
			change = 0.0;
			getshooted = false;
		}
		
	}

	shoot = false;
	m_rotY += m_input->GetMouseDeltaX() * m_turnSpeed * timestep;

	
	// This is the first time a moving object in our game can rotate and change where
	// it's looking. Suddenly moving along the world axes is not useful to us anymore.
	// We need to work out what direction is forward for this particular object. 

	// We'll start by declaring a forward vector in world space
	Vector3 worldForward = Vector3(0, 0, 1);
	Vector3 worldRIGHT = Vector3(1, 0, 0);

	// Next we'll wrap up our Y rotation in a matrix (remember matrices transform vectors)
	Matrix heading = Matrix::CreateRotationY(m_rotY);

	// Finally, we'll transform our world forward vector by the heading matrix which 
	// will essentially spin it from a world forward into a local forward which takes
	// the object's rotation into account.
	Vector3 localForward = Vector3::TransformNormal(worldForward, heading);
	Vector3 localRIGHT = Vector3::TransformNormal(worldRIGHT, heading);


	
	if (m_input->GetKeyHold('W'))
	{
		m_previousPosition = m_position;

		// Move along our local forward vector
		m_position += localForward * m_moveSpeed * timestep;

		if (pass == false) {
			m_position -= 2.0f * localForward;
			pass = true;

		}
	}


	if (m_input->GetKeyHold('S') )
	{

		m_previousPosition = m_position;
		// Move along our local forward vector
		m_position -= localForward * m_moveSpeed * timestep;
		if (!pass) {
			m_position += 2.0f * localForward;
			pass = true;

		}
	}

	if (m_input->GetKeyHold('A') )
	{
		m_previousPosition = m_position;
		// Move left
		m_position -= localRIGHT * m_moveSpeed * timestep;
		if (!pass) {
			m_position += 2.0f * localRIGHT;
			pass = true;

		}
	}
	if (m_input->GetKeyHold('D'))
	{
		 m_previousPosition = m_position;
		// Move right
		m_position += localRIGHT * m_moveSpeed * timestep;
		if (!pass) {
			m_position -= 2.0f * localRIGHT;
			pass = true;

		}
	}
	if (m_input->GetMouseUp(LEFT_MOUSE) && ammo > 0)
	{
		shoot = true;
		ammo -= 1;
	}


	m_distanceTravelled += Vector3::Distance(m_previousPosition, m_position);

	m_previousPosition = m_position;
	m_boundingBox = CBoundingBox(m_position + m_mesh->GetMin(), m_position + m_mesh->GetMax());

}




void Player::OnCapsuleCollisionEnter()
{

}

void  Player::OnCapsuleCollisionStay()
{
}

void  Player::OnCapsuleCollisionExit()
{
}


void Player::OnAmmoCollisionEnter(Ammo* am)
{
	ammo += am->GetAmount();

}

void  Player::OnAmmoCollisionStay(Ammo* am)

{
	ammo += am->GetAmount();

}

void  Player::OnAmmoCollisionExit(Ammo* am)
{
	ammo += am->GetAmount();
}


void Player::OnMonsterCollisionEnter()
{
	SetHp(0);
	collide = true;
}

void  Player::OnMonsterCollisionStay()
{
	SetHp(0);
	collide = true;


}

void  Player::OnMonsterCollisionExit()
{
	SetHp(0);
	collide = true;

}


void Player::OnWallCollisionEnter()
{
	pass = false;
}

void  Player::OnWallCollisionStay()
{
	pass = false;

}

void  Player::OnWallCollisionExit()
{
	pass = false;

}


void Player::OnBulletCollisionEnter(Bullet* bullet)
{
	SetHp(m_HP - bullet->GetDamage());
	getshooted = true;


}

void Player::OnBulletCollisionStay(Bullet* bullet)
{

}

void Player::OnBulletCollisionExit(Bullet* bullet)
{


}

