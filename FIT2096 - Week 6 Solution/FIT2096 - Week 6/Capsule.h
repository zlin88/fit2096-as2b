#ifndef Capsule_H
#define Capsule_H


#include "GameObject.h"

class Capsule : public GameObject
{
private:
	CBoundingBox m_boundingBox;
	bool disappear;

public:
	Capsule(Mesh* mesh, Shader* shader, Texture* texture, Vector3 position);
	~Capsule();

	void Update(float timestep);
	void Render(Direct3D* renderer, Camera* cam);
	CBoundingBox GetBounds() { return m_boundingBox; }
	bool GetDisappear() { return disappear; }
	void OnPlayerCollisionStay();
	void OnPlayerCollisionEnter();
	void OnPlayerCollisionExit();
};



#endif
