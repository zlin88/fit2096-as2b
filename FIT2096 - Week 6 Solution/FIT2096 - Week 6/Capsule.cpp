#include "Capsule.h"
#include "MathsHelper.h"


Capsule::Capsule(Mesh* mesh, Shader* shader, Texture* texture, Vector3 position)
	: GameObject(mesh, shader, texture, Vector3::Zero)
{
	disappear = false;
	m_position = position;
	m_rotX =  PI/ 7.0f;
	m_rotateSpeed = 3.0f;
	m_scaleX = m_scaleY = m_scaleZ = 2.0f;
}


Capsule::~Capsule()
{
}


void Capsule::Update(float timestep) {
	m_rotY -= m_rotateSpeed * timestep / 2;
	m_boundingBox = CBoundingBox(m_position + m_mesh->GetMin(), m_position + m_mesh->GetMax());

}

void Capsule::Render(Direct3D* renderer, Camera* cam)
{
	if (m_mesh)
	{

		m_world = Matrix::CreateScale(m_scaleX, m_scaleY, m_scaleZ) * Matrix::CreateFromYawPitchRoll(m_rotY, m_rotX, m_rotZ) * Matrix::CreateTranslation(m_position);
		m_mesh->Render(renderer, m_shader, m_world, cam, m_texture);
	}

}

void Capsule::OnPlayerCollisionEnter()
{
	disappear = true;

}

void  Capsule::OnPlayerCollisionStay()
{
	disappear = true;
}

void  Capsule::OnPlayerCollisionExit()
{
	disappear = true;

}

