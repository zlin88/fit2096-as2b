#include "CollisionManager.h"
#include<iostream>
using namespace std;
CollisionManager::CollisionManager(std::vector<Bullet*>* mbullets, std::vector<Bullet*>* ebullets, std::vector<Monster*>* monsters,Player**player, std::vector<Capsule*>* capsules, std::vector<Wall*>* walls,std::vector<Ammo*>* ammo)
{
	m_bullets = mbullets;
	E_bullets = ebullets;
	m_monsters = monsters;
	m_player = player;
	m_capsules = capsules;
	m_walls = walls;
	m_ammos = ammo;
	// Clear our arrays to 0 (NULL)
	memset(m_currentCollisions, 0, sizeof(m_currentCollisions));
	memset(m_previousCollisions, 0, sizeof(m_previousCollisions));

	m_nextCurrentCollisionSlot = 0;
}

void CollisionManager::CheckCollisions()
{
	// Check kart to item box collisions
	 BulletToMonster();

	 BulletToPlayer();
	  PlayerToMonster();
      PlayerToWall();
	 PlayerToCapsule();
	  MonsterToWall();
	 PlayerToAmmo();


	// Move all current collisions into previous
	memcpy(m_previousCollisions, m_currentCollisions, sizeof(m_currentCollisions));

	// Clear out current collisions
	memset(m_currentCollisions, 0, sizeof(m_currentCollisions));

	// Now current collisions is empty, we'll start adding from the start again
	m_nextCurrentCollisionSlot = 0;

}

bool CollisionManager::ArrayContainsCollision(GameObject* arrayToSearch[], GameObject* first, GameObject* second)
{
	// See if these two GameObjects appear one after the other in specified collisions array
	// Stop one before length so we don't overrun as we'll be checking two elements per iteration
	for (int i = 0; i < MAX_ALLOWED_COLLISIONS - 1; i += 2)
	{
		if ((arrayToSearch[i] == first && arrayToSearch[i + 1] == second) ||
			arrayToSearch[i] == second && arrayToSearch[i + 1] == first)
		{
			// Found them!
			return true;
		}
	}

	// These objects were not colliding last frame
	return false;
}

void CollisionManager::AddCollision(GameObject* first, GameObject* second)
{
	// Add the two colliding objects to the current collisions array
	// We keep track of the next free slot so no searching is required
	m_currentCollisions[m_nextCurrentCollisionSlot] = first;
	m_currentCollisions[m_nextCurrentCollisionSlot + 1] = second;

	m_nextCurrentCollisionSlot += 2;
}

void CollisionManager::BulletToMonster()
{
	// We'll check each BULLET against every MONSTER
	// Note this is not overly efficient, both in readability and runtime performance
	for (unsigned int i = 0; i < m_bullets->size(); i++)
	{
		for (unsigned int j = 0; j < m_monsters->size(); j++)
		{
			// Don't need to store pointer to these objects again but favouring clarity
			// Can't index into these directly as they're a pointer to a vector. We need to dereference them first
			Bullet* bullet = (*m_bullets)[i];
			Monster* monster = (*m_monsters)[j];

			CBoundingBox BulletBounds = bullet->GetBounds();
			CBoundingBox MonsterBounds = monster->GetBounds();

			// Are they colliding this frame?
			bool isColliding = CheckCollision(BulletBounds, MonsterBounds);
			// Were they colliding last frame?
			bool wasColliding = ArrayContainsCollision(m_previousCollisions, bullet, monster);

			if (isColliding)
			{

				// Register the collision
				AddCollision(bullet, monster);
				if (wasColliding)
				{
					// We are colliding this frame and we were also colliding last frame - that's a collision stay
					// Tell the item box a kart has collided with it (we could pass it the actual kart too if we like)
					monster->OnBulletCollisionStay(bullet);
					bullet->OnMonsterCollisionStay();

				}
				else
				{
					// We are colliding this frame and we weren't last frame - that's a collision enter
					monster->OnBulletCollisionEnter(bullet);
					bullet->OnMonsterCollisionEnter();
				}
			}
			else
			{
				if (wasColliding)
				{
					// We aren't colliding this frame but we were last frame - that's a collision exit
					monster->OnBulletCollisionExit(bullet);
					bullet->OnMonsterCollisionExit();
				}
			}
		}
	}
}


void CollisionManager::PlayerToCapsule() {

	// We'll check PLAYER against every CAPSULE
	// Note this is not overly efficient, both in readability and runtime performance
	for (unsigned int i = 0; i < m_capsules->size(); i++)
	{

		// Don't need to store pointer to these objects again but favouring clarity
		// Can't index into these directly as they're a pointer to a vector. We need to dereference them first
		Capsule* capsule = (*m_capsules)[i];
		Player* player = (*m_player);
		CBoundingBox CapsuleBounds = capsule->GetBounds();
		CBoundingBox PlayerBounds = player->GetBounds();

		// Are they colliding this frame?
		bool isColliding = CheckCollision(CapsuleBounds, PlayerBounds);
		// Were they colliding last frame?
		bool wasColliding = ArrayContainsCollision(m_previousCollisions, capsule, player);

		if (isColliding)
		{

			// Register the collision
			AddCollision(capsule, player);
			if (wasColliding)
			{
				// We are colliding this frame and we were also colliding last frame - that's a collision stay
				// Tell the item box a kart has collided with it (we could pass it the actual kart too if we like)
				capsule->OnPlayerCollisionStay();
				player->OnCapsuleCollisionStay();

			}
			else
			{
				// We are colliding this frame and we weren't last frame - that's a collision enter
				capsule->OnPlayerCollisionEnter();
				player->OnCapsuleCollisionEnter();

			}
		}
		else
		{
			if (wasColliding)
			{
				// We aren't colliding this frame but we were last frame - that's a collision exit
				capsule->OnPlayerCollisionExit();
				player->OnCapsuleCollisionExit();
			}
		}
	}
}

void CollisionManager::PlayerToAmmo() {

	// We'll check PLAYER against every CAPSULE
	// Note this is not overly efficient, both in readability and runtime performance
	for (unsigned int i = 0; i < m_ammos->size(); i++)
	{

		// Don't need to store pointer to these objects again but favouring clarity
		// Can't index into these directly as they're a pointer to a vector. We need to dereference them first
		Ammo* ammo = (*m_ammos)[i];
		Player* player = (*m_player);
		CBoundingBox AmmoBounds = ammo->GetBounds();
		CBoundingBox PlayerBounds = player->GetBounds();

		// Are they colliding this frame?
		bool isColliding = CheckCollision(AmmoBounds, PlayerBounds);
		// Were they colliding last frame?
		bool wasColliding = ArrayContainsCollision(m_previousCollisions, ammo, player);

		if (isColliding)
		{

			// Register the collision
			AddCollision(ammo, player);
			if (wasColliding)
			{
				// We are colliding this frame and we were also colliding last frame - that's a collision stay
				// Tell the item box a kart has collided with it (we could pass it the actual kart too if we like)
				ammo->OnPlayerCollisionStay();
				player->OnAmmoCollisionStay(ammo);

			}
			else
			{
				// We are colliding this frame and we weren't last frame - that's a collision enter
				ammo->OnPlayerCollisionEnter();
				player->OnAmmoCollisionEnter(ammo);

			}
		}
		else
		{
			if (wasColliding)
			{
				// We aren't colliding this frame but we were last frame - that's a collision exit
				ammo->OnPlayerCollisionExit();
				player->OnAmmoCollisionExit(ammo);
			}
		}
	}
}

void CollisionManager::PlayerToMonster() {

	// We'll check PLAYER against every CAPSULE
	// Note this is not overly efficient, both in readability and runtime performance
	for (unsigned int i = 0; i < m_monsters->size(); i++)
	{

		// Don't need to store pointer to these objects again but favouring clarity
		// Can't index into these directly as they're a pointer to a vector. We need to dereference them first
		Monster* monster = (*m_monsters)[i];	
		Player* player = (*m_player);
		CBoundingBox MonsterBounds = monster->GetBounds();
		CBoundingBox PlayerBounds = player->GetBounds();

		// Are they colliding this frame?
		bool isColliding = CheckCollision(MonsterBounds, PlayerBounds);
		// Were they colliding last frame?
		bool wasColliding = ArrayContainsCollision(m_previousCollisions, monster, player);

		if (isColliding)
		{

			// Register the collision
			AddCollision(monster, player);
			if (wasColliding)
			{
				// We are colliding this frame and we were also colliding last frame - that's a collision stay
				// Tell the item box a kart has collided with it (we could pass it the actual kart too if we like)
				monster->OnPlayerCollisionStay();
				player->OnMonsterCollisionStay();

			}
			else
			{
				// We are colliding this frame and we weren't last frame - that's a collision enter
				monster->OnPlayerCollisionEnter();
				player->OnMonsterCollisionEnter();

			}
		}
		else
		{
			if (wasColliding)
			{
				// We aren't colliding this frame but we were last frame - that's a collision exit
				monster->OnPlayerCollisionExit();
				player->OnMonsterCollisionExit();
			}
		}
	}
}



void CollisionManager::PlayerToWall() {

	// We'll check PLAYER against every CAPSULE
	// Note this is not overly efficient, both in readability and runtime performance


	// Don't need to store pointer to these objects again but favouring clarity
	// Can't index into these directly as they're a pointer to a vector. We need to dereference them first

	for (unsigned int i = 0; i < m_walls->size(); i++)
	{
		Wall* wall = (*m_walls)[i];
		Player* player = (*m_player);
		CBoundingBox WallBounds = wall->GetBounds();
		CBoundingBox PlayerBounds = player->GetBounds();

		// Are they colliding this frame?
		bool isColliding = CheckCollision(WallBounds, PlayerBounds);
		// Were they colliding last frame?
		bool wasColliding = ArrayContainsCollision(m_previousCollisions, wall, player);

		if (isColliding)
		{
			// Register the collision
			AddCollision(wall, player);
			if (wasColliding)
			{
				// We are colliding this frame and we were also colliding last frame - that's a collision stay
				// Tell the item box a kart has collided with it (we could pass it the actual kart too if we like)
				wall->OnPlayerCollisionStay();
				player->OnWallCollisionStay();

			}
			else
			{
				// We are colliding this frame and we weren't last frame - that's a collision enter
				wall->OnPlayerCollisionEnter();
				player->OnWallCollisionEnter();

			}
		}
		else
		{
			if (wasColliding)
			{
				// We aren't colliding this frame but we were last frame - that's a collision exit
				wall->OnPlayerCollisionExit();
				player->OnWallCollisionExit();
			}
		}
	}
}


void CollisionManager::MonsterToWall() {

	
	for (unsigned int i = 0; i < m_walls->size(); i++)
	{

		for (unsigned int j = 0; j < m_monsters->size(); j++)
		{

			// Don't need to store pointer to these objects again but favouring clarity
			// Can't index into these directly as they're a pointer to a vector. We need to dereference them first
			Wall* wall = (*m_walls)[i];
			Monster* monster = (*m_monsters)[j];
			CBoundingBox MonsterBounds = monster->GetBounds();

			CBoundingBox WallBounds = wall->GetBounds();

			// Are they colliding this frame?
			bool isColliding = CheckCollision(WallBounds, MonsterBounds);
			// Were they colliding last frame?
			bool wasColliding = ArrayContainsCollision(m_previousCollisions, wall, monster);

			if (isColliding)
			{
				// Register the collision
				AddCollision(wall, monster);
				if (wasColliding)
				{
					// We are colliding this frame and we were also colliding last frame - that's a collision stay
					// Tell the item box a kart has collided with it (we could pass it the actual kart too if we like)
					monster->OnWallCollisionStay();

				}
				else
				{
					// We are colliding this frame and we weren't last frame - that's a collision enter
					monster->OnWallCollisionEnter();

				}
			}
			else
			{
				if (wasColliding)
				{
					// We aren't colliding this frame but we were last frame - that's a collision exit
					monster->OnWallCollisionExit();
				}
			}
		}
	}
}

void CollisionManager::BulletToPlayer()
{
	// We'll check each BULLET against every MONSTER
	// Note this is not overly efficient, both in readability and runtime performance
	for (unsigned int i = 0; i < E_bullets->size(); i++)
	{
			// Don't need to store pointer to these objects again but favouring clarity
			// Can't index into these directly as they're a pointer to a vector. We need to dereference them first
			Bullet* bullet = (*E_bullets)[i];
			Player* player = (*m_player);

			CBoundingBox BulletBounds = bullet->GetBounds();
			CBoundingBox PlayerBounds = player->GetBounds();

			// Are they colliding this frame?
			bool isColliding = CheckCollision(BulletBounds, PlayerBounds);
			// Were they colliding last frame?
			bool wasColliding = ArrayContainsCollision(m_previousCollisions, bullet, player);


			if (isColliding)
			{
			

				// Register the collision
				AddCollision(bullet, player);
				if (wasColliding)
				{
					// We are colliding this frame and we were also colliding last frame - that's a collision stay
					// Tell the item box a kart has collided with it (we could pass it the actual kart too if we like)
					player->OnBulletCollisionStay(bullet);
					bullet->OnPlayerCollisionStay();

				}
				else
				{
					// We are colliding this frame and we weren't last frame - that's a collision enter
					player->OnBulletCollisionEnter(bullet);
					bullet->OnPlayerCollisionEnter();
				}
			}
			else
			{
				if (wasColliding)
				{
					// We aren't colliding this frame but we were last frame - that's a collision exit
					player->OnBulletCollisionExit(bullet);
					bullet->OnPlayerCollisionExit();
				}
			}
		}
	
}

