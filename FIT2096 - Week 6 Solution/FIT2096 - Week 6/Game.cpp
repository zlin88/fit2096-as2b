/*	FIT2096 - Example Code
*	Game.cpp
*	Created by Elliott Wilson & Mike Yeates - 2016 - Monash University
*/

#include<iostream>
#include "Game.h"
#include "TexturedShader.h"
#include "SpinningObject.h"
#include "StaticObject.h"
#include <cmath>
#include "FlyingCamera.h"
#include "ThirdPersonCamera.h"
#include "FirstPersonCamera.h"
#include "DirectXTK/CommonStates.h"
#include <sstream>
#include <time.h>
using namespace std;
Game::Game()
{
	m_renderer = NULL;
	m_currentCam = NULL;
	m_input = NULL;
	m_meshManager = NULL;
	m_textureManager = NULL;
	m_diffuseTexturedShader = NULL;
	m_unlitVertexColouredShader = NULL;
	m_spriteBatch = NULL;
	m_arialFont12 = NULL;
	m_arialFont18 = NULL;
	//m_button = NULL;
	m_player = NULL;
}

Game::~Game() {}

bool Game::Initialise(Direct3D* renderer, InputController* input)
{
	m_renderer = renderer;	
	m_input = input;
	m_meshManager = new MeshManager();
	m_textureManager = new TextureManager();

	if (!InitShaders())
		return false;

	if (!LoadMeshes())
		return false;

	if (!LoadTextures())
		return false;

	LoadFonts();
	InitUI();
	InitGameWorld();
	RefreshUI();
	m_collisionManager = new CollisionManager(&m_bullet,&E_bullet,&m_monsters,&m_player,&m_capsules,&m_Walls,&m_ammos);


	//m_currentCam = new FlyingCamera(m_input, Vector3(0, 4, 0));
	m_currentCam = new FirstPersonCamera(m_input, Vector3(0, 10, -25));

	return true;
}

bool Game::InitShaders()
{
	m_unlitVertexColouredShader = new Shader();
	if (!m_unlitVertexColouredShader->Initialise(m_renderer->GetDevice(), L"Assets/Shaders/VertexShader.vs", L"Assets/Shaders/VertexColourPixelShader.ps"))
		return false;

	m_diffuseTexturedShader = new TexturedShader();
	if (!m_diffuseTexturedShader->Initialise(m_renderer->GetDevice(), L"Assets/Shaders/VertexShader.vs", L"Assets/Shaders/TexturedPixelShader.ps"))
		return false;

	return true;
}

bool Game::LoadMeshes()
{
	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/kart.obj"))
		return false;

	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/ground.obj"))
		return false;

	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/wall.obj"))
		return false;
	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/wall_tile.obj"))
		return false;

	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/rumble_strip.obj"))
		return false;
	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/player_capsule.obj"))
		return false;

	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/enemy.obj"))
		return false;
	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/bullet.obj"))
		return false;
	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/ammoBlock.obj"))
		return false;


	return true;
}

bool Game::LoadTextures()
{
	if (!m_textureManager->Load(m_renderer, "Assets/Textures/kart_red.png"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/kart_green.png"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/grass.jpg"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/rumble_strip.jpg"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/wall.png"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/button.png"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/sprite_star.png"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/ground.png"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/sprite_healthBar.png"))
		return false;
	if (!m_textureManager->Load(m_renderer, "Assets/Textures/bullet.png"))
		return false;
	if (!m_textureManager->Load(m_renderer, "Assets/Textures/gradient_red.png"))
		return false;
	if (!m_textureManager->Load(m_renderer, "Assets/Textures/gradient_redDarker.png"))
		return false;
	if (!m_textureManager->Load(m_renderer, "Assets/Textures/gradient_redLighter.png"))
		return false;
	if (!m_textureManager->Load(m_renderer, "Assets/Textures/gradient_redPink.png"))
		return false;
	if (!m_textureManager->Load(m_renderer, "Assets/Textures/gradient_redOrange.png"))
		return false;
	if (!m_textureManager->Load(m_renderer, "Assets/Textures/sprite_hurtOverlay.png"))
		return false;

	return true;
}

void Game::LoadFonts()
{
	// There's a few different size fonts in there, you know
	m_arialFont12 = new SpriteFont(m_renderer->GetDevice(), L"Assets/Fonts/Arial-12pt.spritefont");
	m_arialFont18 = new SpriteFont(m_renderer->GetDevice(), L"Assets/Fonts/Arial-18pt.spritefont");
}

void Game::InitUI()
{
	m_spriteBatch = new SpriteBatch(m_renderer->GetDeviceContext());

	// Init Buttons
	//m_button = new Button(128, 64, m_textureManager->GetTexture("Assets/Textures/button.png"), L"I'm a button", Vector2(80, 50), m_spriteBatch, m_arialFont12, m_input, [this]
	//{
	//	OnButtonPress();
	//});

	   m_GetHurt = m_textureManager->GetTexture("Assets/Textures/sprite_hurtOverlay.png");
	   for (int i = 0; i < 50; i++) {
		   m_currentItemSprite[i] = m_textureManager->GetTexture("Assets/Textures/sprite_healthBar.png");
	   }
	   for (int i = 0; i < 50; i++) {
		   m_ammoSprite[i] = m_textureManager->GetTexture("Assets/Textures/sprite_healthBar.png");
	   }
}

void Game::OnButtonPress()
{
	MessageBox(0, "Lambda's are awesome!", "It's magic", MB_OK);
}

void Game::RefreshUI()
{
	// Ensure text in UI matches latest scores etc (call this after data changes)
	// Concatenate data into our label string using a wide string stream
	if (m_player)
	{
		std::wstringstream ss;

		// Round to two decimal places for neater output
		ss << "Total score: " << floorf(m_player->GetPoint())<<endl;
		ss << "Bullet left: " << floorf(m_player->GetAmmo());

		m_scoreText = ss.str();
	}
}

void Game::InitGameWorld()
{
	m_player = new Player(m_meshManager->GetMesh("Assets/Meshes/enemy.obj"),
		m_diffuseTexturedShader, Vector3(0, 0.3f, 0),
						  m_input,10,5,50);

	m_gameObjects.push_back(m_player);

	// Static scenery objects
	m_gameObjects.push_back(new StaticObject(m_meshManager->GetMesh("Assets/Meshes/ground.obj"),
		m_diffuseTexturedShader,
		m_textureManager->GetTexture("Assets/Textures/ground.png"), Vector3(0.0,0.0,0.0), 0.5f));
	float width = -70.0f;
	for (int i = 0; i < 71; i++) {
		float length = -70.0;
		for (int j = 0; j < 71; j++) {
			if(width == -70.0 || width == 70.0 || length == -70.0 || length == 70.0)
			m_Walls.push_back(new Wall(m_meshManager->GetMesh("Assets/Meshes/wall_tile.obj"),
				m_diffuseTexturedShader,
				m_textureManager->GetTexture("Assets/Textures/wall.png"), Vector3(width, 0.3f,length), 2.0f));
			length += 2.0f;
		}
		width+= 2.0f;

	}

	for (int i = 0; i < 2; i++) {
		float width = ((rand() / float(RAND_MAX)) * (60 - (-60))) + (-60);
		float length = ((rand() / float(RAND_MAX)) * (60 - (-60))) + (-60);
		m_ammos.push_back(new Ammo(m_meshManager->GetMesh("Assets/Meshes/ammoBlock.obj"),
			m_diffuseTexturedShader, m_textureManager->GetTexture("Assets/Textures/gradient_red.png"), Vector3(width, 0.3f, length)));

	}

	for (int i = 0; i < 6; i++) {

		float width = ((rand() / float(RAND_MAX)) * (55 - (-55))) + (-55);
		float length = ((rand() / float(RAND_MAX)) * (55 - (-55))) + (-55);
		m_capsules.push_back(new Capsule(m_meshManager->GetMesh("Assets/Meshes/player_capsule.obj"),
			m_diffuseTexturedShader, m_textureManager->GetTexture("Assets/Textures/grass.jpg"), Vector3(width, 0.3f, length)));

	}

		for (int i = 0; i <5; i++) {
			float width =((rand() / float(RAND_MAX)) * (50 - (-50))) + (-50);
			float length = ((rand() / float(RAND_MAX)) * (50 - (-50))) + (-50);
			if (i == 0) {
				m_monsters.push_back(new Monster(m_meshManager->GetMesh("Assets/Meshes/enemy.obj"),
					m_diffuseTexturedShader, m_textureManager->GetTexture("Assets/Textures/gradient_red.png"), Vector3(width, 0.3f, length), i+1,m_player->GetPosition(),m_input,7));
			}
			if (i == 1) {
				m_monsters.push_back(new Monster(m_meshManager->GetMesh("Assets/Meshes/enemy.obj"),
					m_diffuseTexturedShader, m_textureManager->GetTexture("Assets/Textures/gradient_redPink.png"), Vector3(width, 0.3f, length), i+1, m_player->GetPosition(), m_input,8));
			}
			if (i == 2) {
				m_monsters.push_back( new Monster(m_meshManager->GetMesh("Assets/Meshes/enemy.obj"),
					m_diffuseTexturedShader, m_textureManager->GetTexture("Assets/Textures/gradient_redOrange.png"), Vector3(width, 0.3f, length), i+1, m_player->GetPosition(), m_input,9));
			}
			if (i == 3) {
				m_monsters.push_back(new Monster(m_meshManager->GetMesh("Assets/Meshes/enemy.obj"),
					m_diffuseTexturedShader, m_textureManager->GetTexture("Assets/Textures/gradient_redLighter.png"), Vector3(width, 0.3f, length), i+1, m_player->GetPosition(), m_input,10));
			}
			if (i == 4) {
				m_monsters.push_back( new Monster(m_meshManager->GetMesh("Assets/Meshes/enemy.obj"),
					m_diffuseTexturedShader, m_textureManager->GetTexture("Assets/Textures/gradient_redOrange.png"), Vector3(width, 0.3f, length), i+1, m_player->GetPosition(), m_input,15));
			}
		}
	
}

void Game::Update(float timestep)
{
	if (m_player->GetHp() == 0 && m_player->Collide()) {

		std::stringstream ss;
		ss << "Monster Beated :" << m_player->GetBeat() << endl
			<< "Player Health :" << m_player->GetHp() << endl
			<< "Total Point :" << m_player->GetPoint() << endl
			<< "Bullet Left :" << m_player->GetAmmo() << endl;

		MessageBox(NULL, ss.str().c_str(), "Defeat, you collide with an enemy", MB_OK);
		exit(EXIT_FAILURE);


	}

	if (m_player->GetHp() == 0) {

		std::stringstream ss;
		ss << "Monster Beated :" << m_player->GetBeat() << endl
			<< "Player Health :" << m_player->GetHp() << endl
			<< "Total Point :" << m_player->GetPoint() << endl
			<< "Bullet Left :" << m_player->GetAmmo() << endl;


		MessageBox(NULL, ss.str().c_str(), "Defeat, your run out of HP", MB_OK);
		exit(EXIT_FAILURE);


	}
	if (m_player->GetAmmo() == 0 && m_ammos.size()==0) {

		std::stringstream ss;
		ss << "Monster Beated :" << m_player->GetBeat() << endl
			<< "Player Health :" << m_player->GetHp() << endl
			<< "Total Point :" << m_player->GetPoint() << endl
			<< "Bullet Left :" << m_player->GetAmmo() << endl;


		MessageBox(NULL, ss.str().c_str(), "Defeat, you dont have any bullet to beat monster", MB_OK);
		exit(EXIT_FAILURE);

	}

	if (m_player->GetHp() >= 0 && m_player->GetBeat() ==5) {
		m_player->SetPoint(m_player->GetPoint() + m_player->GetAmmo());
		std::stringstream ss;
		ss << "Monster Beated :" << m_player->GetBeat() << endl
			<< "Player Health :" << m_player->GetHp() << endl
			<< "Total Point :" << m_player->GetPoint() << endl
			<< "Bullet Left :" << m_player->GetAmmo() << endl;


		MessageBox(NULL, ss.str().c_str(), "Victory��you beat all the monster", MB_OK);
		exit(EXIT_FAILURE);


	}



	m_input->BeginUpdate();

	if (m_player->Shoot() == true ) {
		m_bullet.push_back(new Bullet(m_meshManager->GetMesh("Assets/Meshes/bullet.obj"),
			m_diffuseTexturedShader, m_textureManager->GetTexture("Assets/Textures/bullet.png"), Vector3(m_player->GetPosition().x,m_player->GetPosition().y+1.7f,m_player->GetPosition().z)));
		m_bullet.at(m_bullet.size()-1)->SetYRotation(m_player->GetYRotation());

	}

	if (m_bullet.size()>0) {
		for (int i = 0; i < m_bullet.size(); i++) {
			m_bullet.at(i)->Update(timestep);
			if (m_bullet.at(i)->GetDisappear()|| abs(m_bullet.at(0)->GetPosition().z) - abs(m_bullet.at(0)->GetStartPoint().z) > 20.0f) {
				m_bullet.erase(m_bullet.begin()+i);

			}
		}
		
	}


	

	for (unsigned int i = 0; i < m_gameObjects.size(); i++)
	{
		m_gameObjects[i]->Update(timestep);
	}
	//capsules update
	for (unsigned int a = 0; a < m_capsules.size(); a++)
	{
		m_capsules[a]->Update(timestep);

		if (m_capsules[a]->GetDisappear()) {
			m_capsules.erase(m_capsules.begin() + a);
			srand(time(NULL));
			m_player->SetHp(m_player->GetHp() + rand() % 8 + 1);
		}
	}
	//monster update
	for (unsigned int a = 0; a < m_monsters.size(); a++)
	{
		m_monsters.at(a)->Update(timestep);
		m_monsters.at(a)->SetTargetposition(m_player->GetPosition());


		if (m_monsters.at(a)->Shoot() == true) {
			E_bullet.push_back(new Bullet(m_meshManager->GetMesh("Assets/Meshes/bullet.obj"),
				m_diffuseTexturedShader, m_textureManager->GetTexture("Assets/Textures/bullet.png"), Vector3(m_monsters.at(a)->GetPosition().x, m_monsters.at(a)->GetPosition().y + 1.7f, m_monsters.at(a)->GetPosition().z)));
			E_bullet.at(E_bullet.size() - 1)->SetYRotation(m_monsters.at(a)->GetYRotation());
			E_bullet.at(E_bullet.size() - 1)->SetSpeed(7.0f);

		}

		if (E_bullet.size()>0) {
			for (int i = 0; i < E_bullet.size(); i++) {
				E_bullet.at(i)->Update(timestep);
			}
			if (E_bullet.at(0)->GetPosition().z > E_bullet.at(0)->GetStartPoint().z + 20.0f) {
				E_bullet.erase(E_bullet.begin());
			}
		}





		if (m_monsters.at(a)->GetHp() <= 0) {
			//set value after beat a monster
			m_player->SetPoint(m_player->GetPoint() +  m_monsters.at(a)->GetPoints());
			m_monsters.erase(m_monsters.begin()+a);
			m_player->SetBeat(m_player->GetBeat() + 1);
		}
	}
	// ammo update
	for (unsigned int a = 0; a < m_ammos.size(); a++)
	{
		m_ammos[a]->Update(timestep);

		if (m_ammos[a]->GetDisappear()) {
			m_ammos.erase(m_ammos.begin() + a);
		}
	}


	// Buttons need to update so they can check if the mouse is over them (they swap to a hover section of their image)
	//m_button->Update();

	// We're refreshing our UI every frame as we're assuming the distance travelled text will be changing frequently
	// Usually our UI will only need refreshing when a piece of data we're displaying changes
	RefreshUI();
	

	m_currentCam->SetPosition(Vector3(m_gameObjects[0]->GetPosition().x, m_gameObjects[0]->GetPosition().y+2.0f, m_gameObjects[0]->GetPosition().z) );


	m_collisionManager->CheckCollisions();

	m_currentCam->Update(timestep);

	m_input->EndUpdate();

	for (unsigned int a = 0; a <m_Walls.size(); a++)
	{
		m_Walls.at(a)->Update(timestep);
	}
}

void Game::Render()
{
	m_renderer->BeginScene(0.8f, 1.0f, 0.9f, 1.0f);

	for (unsigned int i = 0; i < m_gameObjects.size(); i++)
	{
		m_gameObjects[i]->Render(m_renderer, m_currentCam);
	}
	for (unsigned int a = 0; a < m_capsules.size(); a++)
	{
		m_capsules[a]->Render(m_renderer, m_currentCam);
	}
	for (unsigned int a = 0; a < m_ammos.size(); a++)
	{
		m_ammos[a]->Render(m_renderer, m_currentCam);
	}
	for (unsigned int a = 0; a < m_monsters.size(); a++)
	{
		
		m_monsters.at(a)->Render(m_renderer, m_currentCam);
	}
	for (unsigned int a = 0; a < m_bullet.size(); a++)
	{
		m_bullet.at(a)->Render(m_renderer, m_currentCam);
	}
	for (unsigned int a = 0; a < E_bullet.size(); a++)
	{
		E_bullet.at(a)->Render(m_renderer, m_currentCam);
	}

	for (unsigned int a = 0; a <m_Walls.size(); a++)
	{
		m_Walls.at(a)->Render(m_renderer, m_currentCam);
	}
	
		
	
		

	
	DrawUI();

	m_renderer->EndScene();		
}

void Game::DrawUI()
{
	// Sprites don't use a shader 
	m_renderer->SetCurrentShader(NULL);

	CommonStates states(m_renderer->GetDevice());
	m_spriteBatch->Begin(SpriteSortMode_Deferred, states.NonPremultiplied());

	// Do UI drawing between the Begin and End calls

	// Tell our button to draw itself
	//m_button->Render();




	// Let's draw some text over our game
	m_arialFont18->DrawString(m_spriteBatch, m_scoreText.c_str(), Vector2(500, 650), Color(0.5f, 0.0f, 0.0f), 0, Vector2(0, 0));
	if (m_player->GetShooted()) {
		m_spriteBatch->Draw(m_GetHurt->GetShaderResourceView(), Color(1.0f, 0.2f, 0.2f));
	}

	// Here's how we draw a sprite over our game
	for (int i = 0; i < m_player->GetHp(); i++) {
		m_spriteBatch->Draw(m_currentItemSprite[i]->GetShaderResourceView(), Vector2(20 + i * 20, 20), Color(0.0f, 0.5f, 0.0f));
	}
	for (int i = 0; i < m_player->GetAmmo(); i++) {
		m_spriteBatch->Draw(m_ammoSprite[i]->GetShaderResourceView(), Vector2(10, 700 - i * 10), Color(0.0f, 0.0f, 0.5f));
	}
	m_spriteBatch->End();
}

void Game::Shutdown()
{
	for (unsigned int i = 0; i < m_gameObjects.size(); i++)
	{
		delete m_gameObjects[i];
	}

	m_gameObjects.empty();

	if (m_currentCam)
	{
		delete m_currentCam;
		m_currentCam = NULL;
	}

	if (m_unlitVertexColouredShader)
	{
		m_unlitVertexColouredShader->Release();
		delete m_unlitVertexColouredShader;
		m_unlitVertexColouredShader = NULL;
	}

	if(m_diffuseTexturedShader)
	{
		m_diffuseTexturedShader->Release();
		delete m_diffuseTexturedShader;
		m_diffuseTexturedShader = NULL;
	}

	if (m_meshManager)
	{
		m_meshManager->Release();
		delete m_meshManager;
		m_meshManager = NULL;
	}

	if (m_textureManager)
	{
		m_textureManager->Release();
		delete m_textureManager;
		m_textureManager = NULL;
	}

	if (m_spriteBatch)
	{
		delete m_spriteBatch;
		m_spriteBatch = NULL;
	}

	if (m_arialFont12)
	{
		delete m_arialFont12;
		m_arialFont12 = NULL;
	}

	if (m_arialFont18)
	{
		delete m_arialFont18;
		m_arialFont18 = NULL;
	}

	/*if (m_button)
	{
		delete m_button;
		m_button = NULL;
	}
	*/
}