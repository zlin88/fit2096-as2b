#include "Bullet.h"
#include <time.h>


Bullet::Bullet(Mesh* mesh, Shader* shader, Texture* texture, Vector3 position):
	GameObject(mesh,shader,texture,position)
{	
	srand(time(NULL));
	m_Sposition = position;
	m_speed = 20.0f;
	m_damage = rand() % 8 + 3;
	disappear = false;

}


void Bullet::Update(float timestep) {

	heading = Matrix::CreateRotationY(m_rotY);
	Vector3 worldForward = Vector3(0, 0, 1);
	Vector3 localForward = Vector3::TransformNormal(worldForward, heading);

	m_position += localForward * m_speed * timestep;

	m_boundingBox = CBoundingBox(m_position + m_mesh->GetMin(), m_position + m_mesh->GetMax());

}



void Bullet::OnMonsterCollisionEnter()
{
	disappear = true;
}

void  Bullet::OnMonsterCollisionStay()
{
	disappear = true;
}

void  Bullet::OnMonsterCollisionExit()
{
	disappear = true;
}

void Bullet::OnPlayerCollisionEnter()
{
	disappear = true;

}

void  Bullet::OnPlayerCollisionStay()
{
	disappear = true;
}

void  Bullet::OnPlayerCollisionExit()
{
	disappear = true;
}



